﻿using Jayway.RobotControl.Logic.Shapes;

namespace Jayway.RobotControl.Test
{
    public abstract class BaseTest
    {
        protected abstract ICircular CreateCircle(int x, int y);
        protected abstract IRectangular CreateRectangular(int x, int y);
    }
}
