﻿using Jayway.RobotControl.Logic.Controller.Enum;
using Jayway.RobotControl.Logic.Shapes;
using NUnit.Framework;

namespace Jayway.RobotControl.Test
{
    [TestFixture]
    public class RoomTest : BaseTest
    {
        protected override ICircular CreateCircle(int x, int y)
        {
            return new Circular(10, x, y);
        }

        protected override IRectangular CreateRectangular(int x, int y)
        {
            return new Rectangular(5, 5, x, y);
        }
        
        [Test]
        public void point_should_be_inside_circle()
        {
            var room = CreateCircle(8,5);
            var result = room.PointIsWithinCircle();
            Assert.IsTrue(result);
        }

        [Test]
        public void point_should_be_outside_circle()
        {
            var room = CreateCircle(11, 3);
            var result = room.PointIsWithinCircle();
            Assert.IsFalse(result);
        }

        [Test]
        // Test will fail
        public void movement_in_circle_should_give_value_set_in_test()
        {
            var room = CreateCircle(0, 0);
            var result = room.ChangeSequenceRobot("HHGVGGVHG", Language.Swedish);

            Assert.AreEqual(3, result.X);
            Assert.AreEqual(1, result.Y);
            Assert.AreEqual("Ö", result.CardinalPointsAbbreviation);
        }

        [Test]
        // Test will fail
        public void movement_in_rectangular_should_give_value_set_in_test()
        {
            var room = CreateRectangular(1, 2);
            var result = room.ChangeSequenceRobot("RFRFFRFRF", Language.English);
            
            Assert.AreEqual(1, result.X);
            Assert.AreEqual(3, result.Y);
            Assert.AreEqual("N", result.CardinalPointsAbbreviation);
        }

        [Test]
        public void movement_in_circle_should_give_value_calculated_on_a_paper()
        {
            var room = CreateCircle(0, 0);
            var result = room.ChangeSequenceRobot("HHGVGGVHG", Language.Swedish);

            Assert.AreEqual(3, result.X);
            Assert.AreEqual(-1, result.Y);
            Assert.AreEqual("Ö", result.CardinalPointsAbbreviation);
        }

        [Test]
        public void movement_in_rectangular_should_give_value_calculated_on_a_paper()
        {
            var room = CreateRectangular(1, 2);
            var result = room.ChangeSequenceRobot("RFRFFRFRF", Language.English);

            Assert.AreEqual(1, result.X);
            Assert.AreEqual(1, result.Y);
            Assert.AreEqual("N", result.CardinalPointsAbbreviation);
        }

        #region old calculation

        /*
        bool PointIsWithinCircle(int radius, int h, int k, int a, int b)
        {
            return (a - h) * (a - h) + (b - k) * (b - k) <= radius * radius;
        }
        */

        #endregion
    }
}