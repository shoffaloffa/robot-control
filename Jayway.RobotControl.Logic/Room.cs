﻿using Jayway.RobotControl.Logic.Controller.Enum;

namespace Jayway.RobotControl.Logic
{
    public class Room : IRoom
    {
        public int X { get; }
        public int Y { get; }
        public IPoint StartPosition { get; }
        public Language Language { get; set; }
        public bool Contains(IPoint position)
        {
            return position != null;
        }
    }
}