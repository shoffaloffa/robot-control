﻿namespace Jayway.RobotControl.Logic
{
    public interface IRoom : IPoint
    {
        IPoint StartPosition { get; }
        bool Contains(IPoint position);
    }
}
