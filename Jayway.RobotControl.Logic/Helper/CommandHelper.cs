﻿using System;
using Jayway.RobotControl.Logic.Controller.Enum;

namespace Jayway.RobotControl.Logic.Helper
{
    public static class CommandHelper
    {
        public static CardinalPoint TurnViewPoint(Command command, CardinalPoint cardinalPoint)
        {
            switch (command)
            {
                case Command.Left:
                    switch (cardinalPoint)
                    {
                        case CardinalPoint.North:
                            return CardinalPoint.West;
                        case CardinalPoint.West:
                            return CardinalPoint.South;
                        case CardinalPoint.South:
                            return CardinalPoint.East;
                        case CardinalPoint.East:
                            return CardinalPoint.North;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(cardinalPoint), cardinalPoint, null);
                    }

                case Command.Right:
                    switch (cardinalPoint)
                    {
                        case CardinalPoint.North:
                            return CardinalPoint.East;
                        case CardinalPoint.East:
                            return CardinalPoint.South;
                        case CardinalPoint.South:
                            return CardinalPoint.West;
                        case CardinalPoint.West:
                            return CardinalPoint.North;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(cardinalPoint), cardinalPoint, null);
                    }

                default:
                    return cardinalPoint;
            }
        }
    }
}
