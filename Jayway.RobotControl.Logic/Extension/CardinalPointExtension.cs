﻿using Jayway.RobotControl.Logic.Controller.Enum;

namespace Jayway.RobotControl.Logic.Extension
{
    public static class CardinalPointExtension
    {
        public static string ToAbbreviation(this CardinalPoint cardinalPoint, Language language)
        {
            return language.Equals(Language.Swedish)
                ? cardinalPoint.ToSwedishAbbreviation()
                : cardinalPoint.ToEnglishAbbreviation();
        }

        public static string ToSwedishAbbreviation(this CardinalPoint cardinalPoint)
        {
            switch (cardinalPoint)
            {
                case CardinalPoint.East:
                    return "Ö";
                case CardinalPoint.South:
                    return "S";
                case CardinalPoint.West:
                    return "V";
                case CardinalPoint.North:
                    return "N";
            }

            return string.Empty;
        }

        public static string ToEnglishAbbreviation(this CardinalPoint cardinalPoint)
        {
            switch (cardinalPoint)
            {
                case CardinalPoint.East:
                    return "E";
                case CardinalPoint.South:
                    return "S";
                case CardinalPoint.West:
                    return "W";
                case CardinalPoint.North:
                    return "N";
            }

            return string.Empty;
        }
    }
}
