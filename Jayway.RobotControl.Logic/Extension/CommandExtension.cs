﻿using Jayway.RobotControl.Logic.Controller.Enum;

namespace Jayway.RobotControl.Logic.Extension
{
    public static class CommandExtension
    {
        private const string SwedishLeft = "V";
        private const string SwedishRight = "H";
        private const string SwedishForward = "G";

        private const string EnglishLeft = "L";
        private const string EnglishRight = "R";
        private const string EnglishForward = "F";

        public static Command ToCommand(this string command)
        {
            switch (command)
            {
                // Swedish
                case SwedishRight:
                    return Command.Right;
                case SwedishLeft:
                    return Command.Left;
                case SwedishForward:
                    return Command.Forward;

                // English
                case EnglishRight:
                    return Command.Right;
                case EnglishLeft:
                    return Command.Left;
                case EnglishForward:
                    return Command.Forward;
            }

            return Command.Stay;
        }
    }
}
