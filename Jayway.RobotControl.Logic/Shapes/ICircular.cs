﻿using Jayway.RobotControl.Logic.Controller;
using Jayway.RobotControl.Logic.Controller.Enum;

namespace Jayway.RobotControl.Logic.Shapes
{
    public interface ICircular : IRoom
    {
        bool PointIsWithinCircle();
        Position ChangeSequenceRobot(string sequence, Language language);
    }
}
