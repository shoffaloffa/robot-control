﻿using Jayway.RobotControl.Logic.Controller;
using Jayway.RobotControl.Logic.Controller.Enum;
using Jayway.RobotControl.Logic.Extension;
using Jayway.RobotControl.Logic.Helper;

namespace Jayway.RobotControl.Logic.Shapes
{
    public class Rectangular : IRectangular
    {
        #region properties
        public int X { get; }
        public int Y { get; }
        private int Width { get; }
        private int Height { get; }
        public IPoint StartPosition { get; }
        public Language Language { get; }
        public CardinalPoint ViewPoint { get; set; }
        #endregion

        public bool Contains(IPoint position)
        {
            return position != null;
        }

        public Rectangular(int width, int height, int x, int y)
        {
            StartPosition = new Point(x, y);
            ViewPoint = CardinalPoint.North;
            Width = width;
            Height = height;
            X = x;
            Y = y;
        }

        public Position ChangeSequenceRobot(string sequence, Language language)
        {
            int robotX = X;
            int robotY = Y;
            char[] splitSequence = sequence.ToCharArray();
            foreach (var seq in splitSequence)
            {
                var command = seq.ToString().ToCommand();
                switch (command)
                {
                    case Command.Forward:
                        switch (ViewPoint)
                        {
                            case CardinalPoint.North:
                                robotY++;
                                break;
                            case CardinalPoint.East:
                                robotX++;
                                break;
                            case CardinalPoint.South:
                                robotY--;
                                break;
                            case CardinalPoint.West:
                                robotX--;
                                break;
                        }
                        break;
                    default:
                        ViewPoint = CommandHelper.TurnViewPoint(command, ViewPoint);
                        break;
                }
            }
            return new Position(robotX, robotY, ViewPoint.ToAbbreviation(language));
        }
    }
}
