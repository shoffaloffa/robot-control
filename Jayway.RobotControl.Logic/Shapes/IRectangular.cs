﻿using Jayway.RobotControl.Logic.Controller;
using Jayway.RobotControl.Logic.Controller.Enum;

namespace Jayway.RobotControl.Logic.Shapes
{
    public interface IRectangular : IRoom
    {
        Position ChangeSequenceRobot(string sequence, Language language);
    }
}