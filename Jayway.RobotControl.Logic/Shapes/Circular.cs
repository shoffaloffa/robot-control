﻿using System;
using Jayway.RobotControl.Logic.Controller;
using Jayway.RobotControl.Logic.Controller.Enum;
using Jayway.RobotControl.Logic.Extension;
using Jayway.RobotControl.Logic.Helper;

namespace Jayway.RobotControl.Logic.Shapes
{
    public class Circular : ICircular
    {
        private const double K = 0;
        private const double H = 0;

        #region properties
        public int X { get; }
        public int Y { get; }
        public int Radius { get; }
        public IPoint StartPosition { get; }
        public Language Language { get; }
        public CardinalPoint ViewPoint { get; set; }
        #endregion

        public bool Contains(IPoint position)
        {
            return position != null;
        }

        public Circular(int radius, int x, int y)
        {
            StartPosition = new Point(x,y);
            ViewPoint = CardinalPoint.North;
            Radius = radius;
            X = x;
            Y = y;
        }

        public bool PointIsWithinCircle()
        {
            return PointIsWithinCircle(X, Y);
        }

        public Position ChangeSequenceRobot(string sequence, Language language)
        {
            int robotX = X;
            int robotY = Y;
            char[] splitSequence = sequence.ToCharArray();
            foreach (var seq in splitSequence)
            {
                var command = seq.ToString().ToCommand();
                switch (command)
                {
                    case Command.Forward:
                        switch (ViewPoint)
                        {
                            case CardinalPoint.North:
                                if (PointIsWithinCircle(robotY+1, robotX))
                                {
                                    robotY++;
                                }
                                break;
                            case CardinalPoint.East:
                                if (PointIsWithinCircle(robotY, robotX+1))
                                {
                                    robotX++;
                                }
                                break;
                            case CardinalPoint.South:
                                if (PointIsWithinCircle(robotY-1, robotX))
                                {
                                    robotY--;
                                }
                                break;
                            case CardinalPoint.West:
                                if (PointIsWithinCircle(robotY, robotX-1))
                                {
                                    robotX--;
                                }
                                break;
                        }
                        break;
                    default:
                        ViewPoint = CommandHelper.TurnViewPoint(command, ViewPoint);
                        break;
                }
            }
            return new Position(robotX, robotY, ViewPoint.ToAbbreviation(language));
        }

        private bool PointIsWithinCircle(int x, int y)
        {
            var r = (Math.Pow(Radius, 2));
            return (Math.Pow(x - H, 2) + Math.Pow(y - K, 2)) <= r;
        }

    }
}