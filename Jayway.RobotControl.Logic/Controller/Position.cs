﻿using Jayway.RobotControl.Logic.Controller.Enum;

namespace Jayway.RobotControl.Logic.Controller
{
    public class Position : IPoint
    {
        public int X { get; }
        public int Y { get; }
        public string CardinalPointsAbbreviation { get; }

        public Position(int x, int y, string cardinalPoint)
        {
            X = x;
            Y = y;
            CardinalPointsAbbreviation = cardinalPoint;
        }
    }
}