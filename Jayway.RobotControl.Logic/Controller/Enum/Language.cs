﻿namespace Jayway.RobotControl.Logic.Controller.Enum
{
    public enum Language
    {
        Swedish,
        English
    }
}