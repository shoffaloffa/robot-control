﻿namespace Jayway.RobotControl.Logic.Controller.Enum
{
    public enum CardinalPoint
    {
        North,
        West,
        East,
        South,
    }
}
