﻿namespace Jayway.RobotControl.Logic.Controller.Enum
{
    public enum Command
    {
        Right,
        Left,
        Forward,
        Stay
    }
}
