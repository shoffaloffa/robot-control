﻿namespace Jayway.RobotControl.Logic
{
    public interface IPoint
    {
        int X { get; }
        int Y { get; }
    }
}