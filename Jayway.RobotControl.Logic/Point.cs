﻿namespace Jayway.RobotControl.Logic
{
    public class Point : IPoint
    {
        public int X { get; }
        public int Y { get; }
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
